```bash
/**
 * Query Compressor concept
 * 
 * Maximum economy at Queries sent to DB, for High Traffic / Big Data systems
 * 
 * Query Compression is a concept with various different implementations. In this approach I show you how to improve RDBMS write performance in case of massive data aggregation
 *
 * (c) https://www.linkedin.com/in/arissc/
 * License: MIT
 */
```
